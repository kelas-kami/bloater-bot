import {Command, Flags} from '@oclif/core'
import Config from '../types/config'
import Bot from '../bot'

export default class RunBot extends Command {
  static description = 'Run telegram bot.'

  static examples = [
    '<%= config.bin %> <%= command.id %> ',
  ]

  static flags = {
    token: Flags.string({char: 't', description: 'Token from bot father.', required: false}),
  }

  static args = [
    {name: 'config', description: 'Configuration file for bot.'},
  ]

  public async run(): Promise<void> {
    const appName = 'bloater_bot'

    const config: Config = {
      appName,
      redisPassword: null,
      redisUsername: null,
      redisHost: process.env.REDIS_HOST || 'localhost',
      redisPort: (process.env?.REDIS_PORT ?? 0) as number || 6379,
      redisDb: (process.env?.REDIS_DB ?? 0) as number || 0,
      botToken: '',
    }

    const {flags} = await this.parse(RunBot)

    if (typeof flags.token === 'string') {
      config.botToken = flags.token
    }

    const botInstance = new Bot(config)

    botInstance.init().start()

    process.once('SIGINT', () => botInstance.stop('SIGINT'))
    process.once('SIGTERM', () => botInstance.stop('SIGTERM'))
  }
}
