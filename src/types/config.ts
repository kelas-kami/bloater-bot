type Config = {
  redisHost: string,
  redisPort: number,
  redisDb: number,
  redisPassword: string | null,
  redisUsername: string | null,
  appName: string,
  botToken: string,
}

export default Config
