import {Context} from 'telegraf'

export default class Greeting {
  // eslint-disable-next-line no-useless-constructor
  constructor(private context: Context) {}

  public async run(): Promise<any> {
    const {from} = this.context

    return this.context.reply(`Hello, @${from?.username}!`)
  }
}
