import {Context, deunionize} from 'telegraf'
import {Debt, DebtType, DebtStatus} from '../../types/debt'
import JournalRepository from '../../repositories/journal-repository'

export default class Record {
  // eslint-disable-next-line no-useless-constructor
  constructor(private context: Context, private journalRepository: JournalRepository) {}

  public async run(): Promise<void> {
    const {message: messageContext} = this.context
    const message = deunionize(messageContext)?.text
    const parsedMessage = message?.split(' ')
    const creditorUsername = (this.context!.from!.username as string)

    if (parsedMessage !== undefined) {
      const debt: Debt = {
        type: DebtType.DEBT,
        status: DebtStatus.ACTIVE,
        username: parsedMessage[1]?.slice(1),
        amount: Number.parseInt(parsedMessage[2], 10),
        currency: 'IDR',
        date: (deunionize(messageContext)?.date as number),
        description: parsedMessage[3],
        creditorUsername: creditorUsername,
      }

      await this.journalRepository.createNewDebt(creditorUsername, debt)

      await this.context.reply('Successfully recorded')
    }
  }
}
