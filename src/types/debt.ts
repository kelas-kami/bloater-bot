enum DebtType {
  DEBT = 'DEBT',
  CREDIT = 'CREDIT',
}

enum DebtStatus {
  ACTIVE = 'ACTIVE',
  CLOSED = 'CLOSED',
}

interface Debt {
  type: DebtType;
  status: DebtStatus;
  username: string;
  amount: number;
  currency: string;
  date: number;
  description: string;
  creditorUsername: string;
}

export {
  DebtType,
  DebtStatus,
  Debt,
}
