import {RedisClientType} from 'redis'
import {Debt, DebtType} from '../types/debt'

export default class JournalRepository {
  // eslint-disable-next-line no-useless-constructor
  constructor(private redisClient: RedisClientType, private appName: string|undefined) {}

  async getDebts(username: string): Promise<Debt[]> {
    const data = []

    data.push(...await this.#getForCreditor(username), ...await this.#getForDebtor(username))

    return data
  }

  async #getForCreditor(creditorUsername: string): Promise<Array<Debt>> {
    const data: Debt[] = []
    const keys = await this.redisClient.keys(`${this.appName}:${creditorUsername}:*`)
    for (const key of keys) {
      // eslint-disable-next-line no-await-in-loop
      const value: string|null = await this.redisClient.get(key)
      if (typeof value === 'string') {
        data.push(JSON.parse(value))
      }
    }

    return data
  }

  async #getForDebtor(debtorUsername: string): Promise<Array<Debt>> {
    const data: Debt[] = []
    const keys = await this.redisClient.keys(`${this.appName}:*:${debtorUsername}:*`)
    for (const key of keys) {
      // eslint-disable-next-line no-await-in-loop
      const value: string|null = await this.redisClient.get(key)
      if (typeof value === 'string') {
        const debt: Debt = JSON.parse(value)
        debt.type = DebtType.CREDIT
        data.push(debt)
      }
    }

    return data
  }

  async createNewDebt(creditorUsername: string, debt: Debt): Promise<void> {
    await this.redisClient.set(`${this.appName}:${creditorUsername}:${debt.username}:${debt.date}`, JSON.stringify(debt))
  }
}
