import {Telegraf, Context} from 'telegraf'
import {createClient, RedisClientType} from 'redis'
import Config from '../types/config'
import Greeting from './actions/greeting'
import Record from './actions/record'
import History from './actions/history'
import JournalRepository from '../repositories/journal-repository'

export default class Bot {
  #redis: RedisClientType
  #telegraf: Telegraf
  journalRepository: JournalRepository

  constructor(config: Config) {
    this.#redis = createClient({
      url: `redis://${config.redisHost}:${config.redisPort}`,
    })

    this.#telegraf = new Telegraf(config.botToken)

    this.journalRepository = new JournalRepository(this.#redis, config.appName)
  }

  init(): Bot {
    this.#telegraf.start((context: Context) => (new Greeting(context)).run())
    this.#telegraf.command('new', (context: Context) => (new Record(context, this.journalRepository)).run())
    this.#telegraf.command('list', (context: Context) => (new History(context, this.journalRepository)).run())

    return this
  }

  async start(): Promise<Bot> {
    await this.#telegraf.launch()
    await this.#redis.connect()

    return this
  }

  stop(reason: string|undefined): void {
    this.#telegraf.stop(reason)
  }
}
