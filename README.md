Bloater Bot 
=================

## Development

### Prerequisites
 - Redis

### Code to see
```
 - src
  - commands/run-bot.ts
  - bot/**/*.ts
  - repositories/*.ts
  - types/*.ts
```

### How to Use 
You need bot access token, refer to the [documentation](https://core.telegram.org/bots) or contact `@botfather` in Telegram to generate a token
> Please ensure redis is running on localhost:6379, otherwise the bot will not work. The custom configuration file is under development.
```shell
$ ./bin/dev run-bot --token <token>
```

After you have done running the run-bot script, head to `Telegram` then search for `kembang_in` in the search bar. Available command listed below :
- `/new`, used to register new debt

- `/list`, used to list all the debt registered in this bot

### Screenshot
- ***user 1*** <br>
![](./art/image_2022-04-07_15-48-19.png)
![](./art/ss-user-1.png)

- ***user 2*** <br>
![](./art/ss-user-2.png)
