import {Context} from 'telegraf'
import {Debt, DebtType} from '../../types/debt'
import JournalRepository from '../../repositories/journal-repository'

export default class History {
  // eslint-disable-next-line no-useless-constructor
  constructor(private context: Context, private journalRepository: JournalRepository) {}

  public async run(): Promise<void> {
    const list: Array<Debt> = []

    if (this.context.from?.username) {
      list.push(...await this.journalRepository.getDebts(this.context.from.username))
    }

    await this.context.replyWithMarkdown(this.#buildText(list))
  }

  #buildText(debts: Debt[]): string {
    // eslint-disable-next-line unicorn/consistent-function-scoping
    const moneyFormat = (amount: number) => (new Intl.NumberFormat()).format(amount)
    // eslint-disable-next-line unicorn/consistent-function-scoping
    const dateFormat = (timestamp: number) => new Date(timestamp * 1000).toLocaleString()

    let text = debts
    .map((item, index) => {
      let text = `${index + 1}. ${item.type === DebtType.CREDIT ? 'you' : item.username}`
      text += item.type === DebtType.CREDIT ?
        ` owes 💰 *${moneyFormat(item.amount)}* to ${item.creditorUsername}, ` :
        ` owed 💰 *${moneyFormat(-item.amount)}*, `

      text += `${item.description} on ${dateFormat(item.date)}`

      return text
    })
    .join('\n\n')

    if (text.length === 0) {
      text = 'you don\'t have any debts, cheers 🍻'
    }

    return text
  }
}
